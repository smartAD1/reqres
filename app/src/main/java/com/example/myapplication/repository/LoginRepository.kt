package com.example.myapplication.repository

import com.example.myapplication.model.response.ReqResResponse
import kotlinx.coroutines.flow.Flow

interface LoginRepository {

    fun setLogin(email: String, pwd: String): Flow<ReqResResponse>

    fun setReg(email: String, pwd: String): Flow<ReqResResponse>
}
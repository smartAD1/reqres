package com.example.myapplication.repository

import com.example.myapplication.api.ReqresService
import com.example.myapplication.model.response.BaseResponse
import com.example.myapplication.model.response.ReqResResponse
import com.example.myapplication.model.response.request.User
import com.example.myapplication.network.RetrofitClient
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow

class LoginRepositoryImp : LoginRepository {

    private val retrofit: ReqresService by lazy {
        RetrofitClient.getRetrofit(ReqresService::class.java)
    }

    override fun setLogin(email: String, pwd: String) = flow {
        val user = User(email, pwd)
        emit(retrofit.login(user).setData().data)
    }

    override fun setReg(email: String, pwd: String) = flow {
            emit(retrofit.reg(email,pwd).setData().data)
        }

    private fun <T> BaseResponse<T>.setData(): BaseResponse<T> {
        return when (code) {
            200 -> BaseResponse(data = data)
            400 -> BaseResponse(error = error, data = data)
            else -> BaseResponse(data = data)
        }
    }
}
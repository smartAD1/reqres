package com.example.myapplication.model.response

import com.google.gson.annotations.SerializedName

data class ReqResResponse(
    @SerializedName("id")
    var id: String = "",
    @SerializedName("token")
    var token: String = "",
    @SerializedName("error")
    var error: String = ""
)
package com.example.myapplication.model.response.request

import com.google.gson.annotations.SerializedName

data class User(
    var email: String = "",
    var pwd: String= ""
)
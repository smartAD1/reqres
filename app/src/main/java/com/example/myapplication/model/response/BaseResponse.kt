package com.example.myapplication.model.response

data class BaseResponse<T>(
    var code: Int = 0,
    var error: String = "",
    var data: T)
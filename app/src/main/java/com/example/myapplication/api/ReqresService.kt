package com.example.myapplication.api

import com.example.myapplication.model.response.BaseResponse
import com.example.myapplication.model.response.ReqResResponse
import com.example.myapplication.model.response.request.User
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface ReqresService {

    @FormUrlEncoded
    @POST("api/login")
    suspend fun login(@Body user: User): BaseResponse<ReqResResponse>

    @FormUrlEncoded
    @POST("/api/register")
    suspend fun reg(@Field("email")email: String, @Field("password")password: String): BaseResponse<ReqResResponse>
}
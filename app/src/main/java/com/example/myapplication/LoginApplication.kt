package com.example.myapplication

import android.app.Application
import com.example.myapplication.repository.LoginRepository
import com.example.myapplication.repository.LoginRepositoryImp
import com.example.myapplication.viewmodel.LoginViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.dsl.module

class LoginApplication: Application() {

    override fun onCreate() {
        super.onCreate()
        val module = module {
            viewModel {
                LoginViewModel(get())
            }
            single<LoginRepository> {
                LoginRepositoryImp()
            }
        }
        startKoin {
            modules(module)
        }
    }
}
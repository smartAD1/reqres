package com.example.myapplication.ui

import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.textfield.TextInputLayout

abstract class BaseActivity: AppCompatActivity() {

    fun TextInputLayout.setEmailReus(checkEmail: Boolean) {
        if (!checkEmail) showMsg("請輸入相關格式")
    }
    fun TextInputLayout.setPwdReus(pwdLength: Int) {
        if (pwdLength < 6) showMsg("密碼長度小於$pwdLength")
    }
    fun showMsg(msg: String) {
        Toast.makeText(this, msg, Toast.LENGTH_LONG).show()
    }
}
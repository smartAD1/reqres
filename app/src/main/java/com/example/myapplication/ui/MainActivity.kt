package com.example.myapplication.ui

import android.os.Bundle
import com.example.myapplication.databinding.ActivityMainBinding
import com.example.myapplication.utils.Status
import com.example.myapplication.viewmodel.LoginViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import java.util.regex.Pattern

class MainActivity : BaseActivity() {

    private lateinit var binding: ActivityMainBinding
    private val loginViewModel: LoginViewModel by viewModel()
    private val emailRegex = Pattern.compile("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                        "\\@" +
                        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                        "(" +
                        "\\." +
                        "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                        ")+")


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        initView()
        initVM()
    }

    private fun initView() = binding.run {
        btnLogin.setOnClickListener {
            val checkEmail = emailRegex.matcher("${email.text}").matches()
            if (checkEmail && pwd.text.length >= 6) {
                loginViewModel.setLogin("${email.text}", "${pwd.text}")
            }
            emailInput.setEmailReus(checkEmail)
            pwdTextInput.setPwdReus(pwd.length())
        }
        btnRegister.setOnClickListener {
            val checkEmail = emailRegex.matcher("${email.text}").matches()
            if (checkEmail && pwd.text.length >= 6) {
                loginViewModel.setReg("eve.holt@reqres.in", "pistol")
            }
            emailInput.setEmailReus(checkEmail)
            pwdTextInput.setPwdReus(pwd.length())
        }
    }

    private fun initVM() = loginViewModel.run {
        loginLiveData.observe(this@MainActivity) {
            when (it) {
                is Status.TimeOut -> showMsg(it.msg)
                is Status.Failure -> showMsg("${it.exception.message}")
                is Status.Success -> showMsg("login Success")
            }
        }
        regLiveData.observe(this@MainActivity) {
            when (it) {
                is Status.TimeOut -> showMsg(it.msg)
                is Status.Failure -> showMsg("${it.exception.message}")
                is Status.Success -> showMsg("register Success")
            }
        }
    }
}
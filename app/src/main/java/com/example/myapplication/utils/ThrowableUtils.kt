package com.example.myapplication.utils

fun Throwable.setException() =
    if ("$message".contains("Timeout")) Status.TimeOut("$message") else Status.Failure(this)
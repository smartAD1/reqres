package com.example.myapplication.utils

import com.example.myapplication.model.response.ReqResResponse
import java.sql.Time


sealed class Status<out T> {
    class Success<T>(val data: T) : Status<T>()
    data class Failure(val exception: Throwable) : Status<Nothing>()
    data class TimeOut(val msg: String): Status<Nothing>()
}
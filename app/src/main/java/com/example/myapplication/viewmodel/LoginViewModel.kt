package com.example.myapplication.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.myapplication.model.response.ReqResResponse
import com.example.myapplication.repository.LoginRepository
import com.example.myapplication.repository.LoginRepositoryImp
import com.example.myapplication.utils.Status
import com.example.myapplication.utils.io
import com.example.myapplication.utils.setException
import com.example.myapplication.utils.ui
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch

class LoginViewModel(private val loginRepository: LoginRepository) : ViewModel() {

    val loginLiveData = MutableLiveData<Status<ReqResResponse>>()
    val regLiveData = MutableLiveData<Status<ReqResResponse>>()

    fun setLogin(email: String, pwd: String) = viewModelScope.launch {
        io {
            loginRepository.setLogin(email, pwd).catch {
                ui { loginLiveData.value = it.setException() }
            }.collect {
                ui { loginLiveData.value = Status.Success(it) }
            }
        }
    }

    fun setReg(email: String, pwd: String) = viewModelScope.launch {
        io {
            loginRepository.setReg(email, pwd).catch {
                ui { regLiveData.value = it.setException() }
            }.collect {
                ui { regLiveData.value = Status.Success(it) }
            }
        }
    }
}
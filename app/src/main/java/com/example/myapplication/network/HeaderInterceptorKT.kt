package com.example.myapplication.network

import okhttp3.Interceptor
import okhttp3.Response

class HeaderInterceptorKT: Interceptor {

    companion object {
        private const val HEADER_CONTENT_TYPE = "Content-Type"
    }

    override fun intercept(chain: Interceptor.Chain): Response {
        val originalRequest = chain.request().newBuilder()
        originalRequest.addHeader(HEADER_CONTENT_TYPE, "application/x-www-form-urlencoded")
        return chain.proceed(originalRequest.build())
    }
}
package com.example.myapplication

import org.junit.After
import org.junit.Before
import org.junit.Test
import org.junit.Assert.*
import java.util.regex.Pattern


class TestMatcherEmail {
    var emailRegex: Pattern? = null

    @Before
    fun setUp() {
        emailRegex = Pattern.compile("[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                "\\@" +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                "(" +
                "\\." +
                "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                ")+")
    }

    @Test
    fun matcherEmail() {
        val email = "eve.holt@reqres.in"
        assertEquals(true,emailRegex?.matcher(email)?.matches())
    }

    @After
    fun done() {
        emailRegex = null
    }
}